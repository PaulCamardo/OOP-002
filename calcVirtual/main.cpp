// Paul Camardo
// Calculator implementation with Virtual functions

#include "calc.hpp"

#include <iostream>

// function declaration, i put in calc.cpp 
std::ostream& operator<<(std::ostream& os, const Expr* e);


int 
main() {
  // (10 / 2) * (9 + 2)
  Expr* e = new Mul(
    new Div(new Int(10), new Int(2)),
    new Add(new Int(9), new Int(2))
  );

  std::cout << e << " == " << e->evaluate() << '\n';

 // not sure why this is here or whats it doing
  Expr* e2 = e->reduce();
  std::cout << e2 << " == " << e2->reduce() << "\n";

  Expr *e3 = e2->reduce();
  std::cout << e3 << " == " << e3->reduce() << "\n";


  

  delete e;
  delete e2;
  delete e3;

return 0;
}

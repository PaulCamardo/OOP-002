// Paul Camardo
// Calculator implementation with Virtual functions

#include "calc.hpp"


// overloaded comparison "=="
bool operator==(const Expr& e1, const Expr& e2)
{
  // If e1 and e2 are identical, then they're equal.
  if (&e1 == &e2)
    return true;

  // Otherwise, call a virtual function to "discover" the
  // dynamic type of e1.
  return e1.equal(&e2);
}

// overloading ostream operator, why 
std::ostream& operator<<(std::ostream& os, const Expr* e)
{
  e->print(os);
  return os;
}

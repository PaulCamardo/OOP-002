// Paul Camardo
#include "json.hpp"

#include <iostream>
#include <iterator>
#include <string>

int 
main() {

  // [null, true, false, []].
  Value* v = new Array {
    new Null{},
    new Bool{true},
    new Bool{false},
    new Array{}
  };
  v->print();

  // https://stackoverflow.com/questions/2602013/read-whole-ascii-file-into-c-stdstring
  std::string str((std::istreambuf_iterator<char>(std::cin)),
                   std::istreambuf_iterator<char>());

  // can't use until parse is actually written
  // Value* v = parse(str);
  // v->print();
  delete v;
}


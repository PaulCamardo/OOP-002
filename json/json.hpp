//Paul Camardo

#include <iostream>
#include <iomanip>
#include <map>
#include <vector>


// This is an object containing a number of sub-objects.
struct Value
{
  
  // virtual destructor.
  virtual ~Value() { }

  // virtual print function
  virtual void print() const = 0;
};

// Represents the value 'null'.
struct Null : Value
{
  Null()
  { }

  void print() const { 
    std::cout << "null"; 
  }
};

// Represents the boolean values 'true' and 'false'.
struct Bool : Value
{
  Bool(bool b)
    : val(b)
  { }

  //print function
  void print() const { 
    if (val)
      std::cout << "true";
    else
      std::cout << "false";
  }
  
  bool val;
};

/// Represents numeric (floating point values).
struct Number : Value
{
  Number(double d)
    : val(d)
  { }

  void print() const override {
    std::cout << val;
  }

  double val;
};

/// Represents string values.
struct String : Value
{
  String(const std::string& s)
    : str(s)
  { }

  String(const char* first, const char* limit)
    : str(first, limit)
  { }

  void print() const { 
    std::cout << str; 
  }
  
  std::string str;
};

/// Recursive part of data structure.
struct Array : Value
{
  // Constructs an empty array.
  Array() = default;

  Array(std::initializer_list<Value*> list)
    : vals(list)
  { }

  ~Array() override { 
    for (Value* v : vals)
      delete v;
  }

  void add(Value* v) {
    vals.push_back(v);
  }

  void print() const override {
    std::cout << '[';
    for (auto iter = vals.begin(); iter != vals.end(); ++iter) {
      (*iter)->print();
      if (std::next(iter) != vals.end())
        std::cout << ", ";
    }
    std::cout << "]";
  } 
  
  std::vector<Value*> vals;
};

/// 
struct Object : Value
{
  Object() = default;

  ~Object() {
    for (auto p : fields) {
      delete p.first;
      delete p.second;
    }
  }

  void print() const override {
    std::cout << '{';
    for (auto iter = fields.begin(); iter != fields.end(); ++iter) {
      iter->first->print();
      std::cout << " : ";
      iter->second->print();
      if (std::next(iter) != fields.end())
        std::cout << ", ";
    }
    std::cout << "}";
  }

  void add(String* k, Value* v) {
    fields.emplace(k, v);
  }
  
  // FIXME: Compare string values indirectly.
  //not sure exactly what this does
  std::map<String*, Value*> fields;
};


// need to write a parse to actually use it, couldn't find one anywhere else
//Value* parse(const std::string& str);

// (c) Paul Camardo
// All Rights Reserved

#include "card.hpp"


// Pragmatic approach. Don't write a class if you don't
// need to.
using Deck = std::vector<Card>;
using Player = std::vector<Card>;
using Pile = std::vector<Card>;
// Make a standard deck of cards.
Deck make_standard_deck();

// Combine two decks of cards.
Deck make_combined_deck(const Deck& d1, const Deck& d2);

// Shuffle a deck of cards.
void shuffle(Deck& d);

// Print a deck of cards.
void print(const Deck& d);

// deal
void deal(Deck& deck, Player& p1, Player& p2);

//put into spoils
void put(Player& player, Pile& battleGround);
//take from spoils
void take(Player& player, Pile& battleGround);


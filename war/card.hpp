// (c) Paul Camardo
// All Rights Reserved

#pragma once


#include <utility>
#include <iosfwd>


// enumeration type
// ace is low and starts at 1, kind is 13
enum Rank 
{
	Ace,
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
};

enum Suit
{
	Hearts,
	Diamonds,
	Clubs,
	Spades,
};

class Card
{
public:
	//create a default card, my compiler doesn't like card()= default;
	Card();

	//making a card with a rank and suit
	Card(Rank r, Suit s)
		: rank(r), suit(s) // memeber initializer 
	{ }

	//Acessor Fucntions
	Rank get_rank() const { return rank; }
	Suit get_suit() const { return suit; }

private:
	Rank rank;
	Suit suit;

};

// Equality comparison overloading operators
bool operator==(Card a, Card b);
bool operator!=(Card a, Card b);

// Ordering, overloading opertors
bool operator<(Card a, Card b);
bool operator>(Card a, Card b);
bool operator<=(Card a, Card b);
bool operator>=(Card a, Card b);

std::ostream& operator<<(std::ostream& os, Card c);
std::ostream& operator<<(std::ostream& os, Rank r);
std::ostream& operator<<(std::ostream& os, Suit s);










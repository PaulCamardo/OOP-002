// (c) Paul Camardo
// All Rights Reserved

#include "game.hpp"

// Game deck (not sure which or how many cards).
  // Contains 2*n cards.
  Deck deck;
  deck.shuffle();

  Player p1;
  Player p2;

  // Round-robin dealing.
  while (!deck.empty()) {
    deal_one(deck, p1);
    deal_one(deck, p2);
  }

  // The spoils pile.
  Pile spoils;

  while (true) {
    if (p1.empty()) {
      if (p2.empty())
        std::cout << "TIE\n";
      else
        std::cout << "P2 WINS\n";
      break;
    }
    else if (p2.empty()) {
      std::cout << "P1 WINS\n";
      break;
    }

    Card c1 = p1.take();
    Card c2 = p2.take();

    spoils.add(c1);
    spoils.add(c2);

    if (c1 > c2) {
      give(p1, spoils);
    }
    else if (c2 > c1) {
      give(p2, spoils);
    }
    else {
      // TODO: Need to check number of cards.
      // TODO: Number of sacrifices...
      // TODO: Negotiate sacrifices...

      // WAR!
      spoils.add(p1.take());
      spoils.add(p2.take());
      continue;      
    }
    assert(spoils.empty());
  }


//random code not wanted now but maybe useful
  // Split the deck.
  // deal(deck, p1, deck.size() / 2);
  // deal(deck, p2, deck.size());


  // Very object-oriented.
  //
  // Creates an artificial dependency from Deck to Player.
  // Inhibit reusability of the Deck class.
  // deck.deal(p1, deck.size() / 2);
  // deck.deal(p2, deck.size());
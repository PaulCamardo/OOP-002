// (c) Paul Camardo
// All Rights Reserved

//source file included only once
#pragma once

// Nested class?
struct Options
{
  int num_decks = 1;
  bool ace_high = true;
  int num_sacrifice = 1;
  bool negotiable_sacrifice = true;
};

struct Game
{
  Options options;
  Deck deck;
  Player p1;
  Player p2;
  // spoils pile is a vector?
  Pile pile;
  int turn;
};



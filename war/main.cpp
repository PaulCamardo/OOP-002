// (c) Paul Camardo
// All Rights Reserved
//

#include "card.hpp"
#include "deck.hpp"
//#include "game.hpp"

#include <iostream>
#include <iomanip>
#include <vector>
#include <random>

//random number generators
std::random_device rng; //used to seed
std::minstd_rand prng;


int main()
{
// this will be in game mode
	//seeding prng with rng to get a new random number each time
	prng.seed(rng());
	//making a deck of cards
    Deck d1 = make_standard_deck();
  	//shuffle
  	shuffle(d1);
  	//create players as vectors
  	Player p1;
  	Player p2;

  	//dealing decks
  	deal(d1, p1, p2);
  	print(p1);
  	print(p2);
  
  	//creating the battleground or spoils pile
  	Pile battleGround;

  	//creating counting variables
  	int numberOfGames = 0;
  	int numberOfTies = 0;

  	while (true) 
  	{
  		numberOfGames++;
	    if (p1.empty()) 
	    {
	      if (p2.empty())
	        std::cout << "TIE\n";
	      else
	        std::cout << "P2 WINS\n";
	      break;
	    }
	    else if (p2.empty()) 
	    {
	      std::cout << "P1 WINS\n";
	      break;
    	}
    

	  	// each player puts a card in the spoils pile
		put(p1, battleGround);
		print(battleGround);
		std::cout<< " :Player 1 put\n";
		put(p2, battleGround);
		print(battleGround);
		std::cout<< " :Player 2 put\n";


		if(battleGround[0] < battleGround[1])
		{
			//shuffle(battleGround); // get random 
			int size = battleGround.size();
			for(int i = 1; i <= size; i++)
			{
				take(p2, battleGround);
			}
			std::cout << "here2\n";	

		}
		else if (battleGround[0] > battleGround[1])
		{
			//shuffle(battleGround);
			int size = battleGround.size();
			for(int i = 1; i <= size; i++)
			{
				take(p1, battleGround);
			}
			std::cout << "here1\n";	
		}
		else
		{
			numberOfTies++;
			//these are sacrificed, each killing two cards
			put(p1, battleGround);
			put(p2, battleGround);
			continue;
		}


	}

std::cout << "Number of ties: " << numberOfTies << std::endl;
std::cout << "Number of rounds: " << numberOfGames << std::endl;


  	return 0;
}
/* Think about how to play

Dealing cards fucntions: you deal every other so evens and odds not 1-26 and 27-52
	use a vector that allows you to add up to 52 cards

play function: a function that holds below that uses recursion to complete the game, 
	it has a check on each players hand to see if empty. if empty other player wins

Play top card function: each player puts top card down checks for less than 
	if rank are the same go to Tie function
Tie function: Players put down one card face down and the next card is tie breaker, use less than fucntion
	if tied again, just use recursion so it can go indefinetly
Winner gets cards: add cards to winner pile

Keeping track 
	- put a counter in play top card function, serves as number of rounds
	- in winner get cards count the number of cards handed over cumlutive
	- output these in play function number of round: and number of cards exchanged

Other
	if you want the users to actually play the game we'll need to keep track of the number 
of cards in pile as well as state winner and loser of each round as well as show the card,
if simulation then not needed

	A way to make sure game doesn't go forever is to keep played in a seperate vector and once the
orginal is done you shuffle the played deck. Now the game should end, there's still a very
small possibility it doesnt, but its negible 


*/

/*
other stuff i want to keep
//		Two more ways to sort, just using a function created above, 
// 		std::sort(deck.begin(), deck.end(), [](Card a, Card b) 
// 		{ // lambda expression
//    		return a > b;
// 	 	});
//   	 std::sort(deck.begin(), deck.end(), [](Card a, Card b) 
//			{ // lambda expression
//    			return a.get_suit() < b.get_suit();
// 			 });
	//std::shuffle is from the stl, randomizes the order
	std::shuffle(deck.begin(), deck.end(), prng);
	//one way to sort a deck
	std::sort(deck.begin(), deck.end(), card_greater);


*/






